import os
import re
import time
import errno
import shutil
import logging
import grequests
from gevent import monkey
import subprocess as sp
from datetime import datetime

monkey.patch_all()

'''
Common Intradb Pipline elements.
'''


class Pipeline:
    def __init__(self, xnat, opts, name="NONAME"):
        self.xnat = xnat
        self.opts = opts
        self.name = name
        self._setup()

    def _setup(self):
        """
        Setup pipeline logs and build space
        """
        self.now = self.launch_time = datetime.now().strftime("%Y%m%d_%H%M%S")
        self.build_dir = os.path.join(
            self.opts.build, self.opts.project, self.opts.session, self.now)
        self.mkdir_p(self.build_dir)
        self.log_dir = os.path.join(
            self.opts.log, self.name, self.opts.session, self.now)
        self.log = logging.getLogger()
        self.mkdir_p(os.path.join(self.log_dir, 'scans'))
        formatter = logging.Formatter(
            '%(asctime)s - %(levelname)s - %(message)s')
        # print self.name, self.opts.session
        main_log_file = os.path.join(self.log_dir, "{}_{}.log".format(
            self.name, self.opts.session))
        filehandler = logging.FileHandler(main_log_file, 'w')
        filehandler.setFormatter(formatter)
        streamhandler = logging.StreamHandler()
        self.log.addHandler(filehandler)
        self.log.addHandler(streamhandler)
        self.log.setLevel(logging.DEBUG)
        self.launch_time = datetime.now()
        self.return_statuses = []
        self.sleep_time = 60
        self.max_jobs = 200

    def submit(self, scan, job, jobname="", queue="hcp_standard.q"):
        """
        Submit the job for the scan using subprocess and
        write the output to a scan log that will later be combined
        """
        # Sleep until we're under an acceptable SGE client count
        while self.getSgeClientCount() > self.max_jobs:
            msg = "More than {} SGE clients running. Sleeping for {}".format(
                self.max_jobs, self.sleep_time)
            self.log.debug(msg)
            time.sleep(self.sleep_time)

        scan_log = os.path.join(self.log_dir, 'scans')

        if not jobname:
            jobname = "{}-{}-{}".format(self.name, scan, self.opts.session)

        # Prepend runtime environment to command based on submission option
        if self.opts.execute == 'sge':
            # -sync y will wait for the job to complete before exiting
            job = "qsub -sync y -q {q} -o {o} -e {e} -N {name} {cmd}".format(
                q=queue, o=scan_log, e=scan_log, name=jobname, cmd=job)
            self.log.info("Submitting {name} to SGE for scan {scan}".format(
                name=jobname, scan=scan))
        elif self.opts.execute == 'local':
            if not job.startswith('/'):
                job = "./" + job
            self.log.info("Running {} process for scan {}".format(
                jobname, scan))

        self.log.info(job)

        log_name = '{}_{}.log'.format(self.name, scan)
        scan_log_file = os.path.join(self.log_dir, 'scans', log_name)

        with open(scan_log_file, 'a') as scan_log:
            status = sp.call(job.split(), stdout=scan_log, stderr=scan_log)
            self.return_statuses.append(status)

            scan_log.write(job)
            print("\nRETURN STATUS for {}: {}".format(jobname, status))
            scan_log.write("\n\nqsub jobs currently running: {}\n".format(
                self.getSgeClientCount()))
        #return status

    def deleteExistingResources(self, resource, scans):
        """
        Submit all resource delete requests in parallel
        """
        self.log.info("Deleting existing {} resources for {}".format(
            resource, ','.join(scans)))
        urls = []

        for scan in scans:
            uri = "/data/projects/{}/subjects/{}/experiments/{}/scans/{}/resources/{}?removeFiles=true".format(
                self.xnat.project,
                self.xnat.subject_label,
                self.xnat.session_label,
                scan,
                resource
            )
            urls.append(self.xnat.url + uri)

        # Reuse the same session for all requests
        rs = (grequests.delete(u, session=self.xnat.session) for u in urls)
        results = grequests.map(rs)

        for r in results:
            r.raise_for_status

    def combineScanLogs(self):
        scan_log_dir = os.path.join(self.log_dir, 'scans')
        scan_logs = self.natural_sort(os.listdir(scan_log_dir))
        log_file = os.path.join(self.log_dir, 'combined-scans.log')

        with open(log_file, 'w') as outfile:
            for slog in scan_logs:
                slog_path = os.path.join(scan_log_dir, slog)
                with open(slog_path) as infile:
                    outfile.write('---\n')
                    outfile.write(slog + '\n\n')
                    outfile.write(infile.read())
                    outfile.write('---\n\n')

    def setPcpStatus(self, pcp_pipeline_id, status):
        self.log.info("Setting PCP status: " + status)
        uri = "/xapi/pipelineControlPanel/project/" + \
            "{}/pipeline/{}/entity/{}/group/ALL/setValues?status={}".format(
            self.xnat.project, pcp_pipeline_id, self.xnat.session_label, status)
        try:
            self.xnat.post(uri, json={})
        except Exception as e:
            self.log.warn("Couldn't set PCP status -- " + str(e))

    def setPcpEndStatus(self, pcp_pipeline_id):
        # Check the return status of all submitted scan threads
        # This needs to be called after all threads have returned

        pcpStatus = 'COMPLETE'

        for status in self.return_statuses:
            if status != 0:
                pcpStatus = 'ERROR'
                break

        if not self.return_statuses:
            pcpStatus = 'UNKNOWN'

        self.log.info("PCP end status: " + pcpStatus)
        self.setPcpStatus(pcp_pipeline_id, pcpStatus)
        
        #message += "<br>Launched: {}".format(str(self.launch_time))
        elapsed_message = "<br><b>Elapsed:</b> {}<br>".format(
            datetime.now()-self.launch_time)

        uri = "/xapi/pipelineControlPanel/project/" + \
            "{}/pipeline/{}/entity/{}/group/ALL/setValues?statusInfo={}".format(
            self.xnat.project, pcp_pipeline_id, self.xnat.session_label, elapsed_message)
        try:
            self.xnat.post(uri, json={})
        except Exception as e:
            self.log.warn("Couldn't set PCP status -- " + str(e))

    def deleteBuildDir(self):
        if sum(self.return_statuses) == 0:
            shutil.rmtree(self.build_dir)

    def mkdir_p(self, path):
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    @staticmethod
    def getSgeClientCount():
        # qconf -secl | grep qsub | wc -l
        command = 'qconf -secl'
        output = sp.check_output(command.split())
        return str(output).count('qsub')

    @staticmethod
    def natural_sort(l):
        convert = lambda text: int(text) if text.isdigit() else text.lower()
        alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
        return sorted(l, key=alphanum_key)
